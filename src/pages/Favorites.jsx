import { ItemList } from "../components/ItemsList/ItemsList";

export function Favorites() {
  return (
    <>
      <h3 className="text-center pt-3 pb-2">Favorite items:</h3>
      <ItemList isFav={true} />
    </>
  );
}
