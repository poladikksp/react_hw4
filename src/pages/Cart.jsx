import { ItemList } from "../components/ItemsList/ItemsList";

export function Cart() {
  return (
    <>
      <h3 className="text-center pt-3 pb-2">Your cart:</h3>
      <ItemList isCart={true} />
    </>
  );
}
